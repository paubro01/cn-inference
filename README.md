
# Building and Deploying the ML inference container

## Introduction
This document guides the user to build and deploy the ML inference container.

## Pre-Requisites
- ARM System Ready Device.
- USB Pen Drive Flashed with ARM System-Ready EWAOL Image with preinstalled docker.

It is recommended to build and run this container after the Video Capture container and before the Application container.

## Steps overview
- Setup for the ML inference container on Edge device
    - Clone the repository
    - Building the docker container
    - Running the docker container
- Deployment of the ML inference container from the cloud
    - AWS cloud
        - Create an EC2 instance
        - Connect to the EC2 instance
        - Deploying the ML inference container on the Edge instance
        - Stop the ECS instance
    - Alibaba cloud
        - Create an ECS instance
        - Connect to the ECS instance
        - Deploying the ML inference container on the Edge instance
        - Release the ECS instance
- What is next ?

The user can deploy the ML inference container from an edge device or from a cloud instance. Follow either **Setup for the ML inference container on Edge device** or **Deployment of the ML inference container from the cloud** depending on your needs.

## Setup for the ML inference container on Edge device
Run the below commands on the target device (ARM System Ready Device).

### Clone the repository
- Clone the gitlab repo using the below command.
```
git clone -b <BRANCH_NAME> <PROJECT_GIT_URL>
```

### Building the docker container
- Change the working directory to the ML inference container folder.
```sh
cd <PROJECT_ROOT>/
```
- Build the docker container.
    ```sh
    docker build -t <IMAGE_NAME>:<IMAGE_TAG> .
    ```
    Mention the image name & tag name of your choice.

- Check the Docker Images list with the command below.
```sh
docker images
```

### Running the docker container
- Use the below command to launch the ML inference container.
    ```sh
    docker run -it --network=host <IMAGE_NAME>:<IMAGE_TAG> -p <INF_PORT> -m <MODEL> -c <CPU_CORES_OF_DEVICE>
    ```
    Options:
    - -p: Port number where the ML inference container should run. Preferred port number is 8080 and should be given when running the Application container (cn-application project).
    - -m: Model name that should be loaded for inference. Currently **yolov3** and **tiny_yolov3** models are supported.
    - -c: Number of CPU cores available in the device. This argument is optional and has the default value of **4**.

## Deployment of the ML inference container from the cloud
The following steps are used to deploy the ML inference container from the cloud. Please choose the cloud service provider of your choice. Only AWS and Alibaba are currently supported.

### AWS cloud
#### **Create an EC2 instance**
- Open the AWS Cloud console and login to the cloud service.
- Search and access the **EC2** service with the search bar.
- On EC2 service console page:
    - On the top right corner, select the preferred region from the dropdown menu.
    - On the left side menu, under **Instances**, click on **Instances**.
- On the **Instances** page, click on **Launch instances**.
- On the **Launch instances** page:
    - Provide a name of user choice for the instance.
    - Under **Application and OS Images (Amazon Machine Image)** section, under **Quick Start**, select **Ubuntu** as preferred OS. An AMI should be automatically selected. Change it only if you have specific needs.
        - Select the **Architecture** as **64-bit (Arm)**.
    - Under **Instance type** section, select an instance with a minimum of 4 vCPU and 8 GB memory.

        Preferred instance would be **t4g.xlarge**.
    - Under **Key pair (login)** section, click on **Create new key pair**.
        - On the **Create key pair** window, provide a key pair name.
        - Key pair type: **RSA**.
        - Private key file format: **.pem** .
        - Click on **Create key pair**.

            This will download a file which will be used to access the EC2 instance.
    - Under **Configure storage** section, type the storage as 10GB.
    - Keep all the other options as default and click on **Launch instance**.

A new EC2 instance should now get created and start running. The status of the EC2 instance can be monitored from the **Instances** page of the EC2 AWS cloud console.

#### **Connect to the EC2 instance**
There are multiple ways to connect to an EC2 instance.
- Please refer to [Connect to your Linux Instance](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/AccessingInstances.html) and follow the preferred connecting method.

To check connectivity to the EC2 instance:
- Access the **Instances** page of the EC2 AWS cloud console.
- Select the EC2 instance created in the list displayed.
- From the dashboard appearing, find the "Public IPv4" information.
- Ping the EC2 instance.
    ```sh
        ping <ec2-instance-public-ip>
    ```

To set up correctly the key pair:
- Find the downloaded key pair *.pem file and change the permission.
    ```sh
    chmod 400 <key-pair-name>.pem
    ```
Connecting into the EC2 instance using SSH:
```sh
ssh -i <path_to_key_pair_.pem_file> ubuntu@<ec2-instance-public-ip>
```

Troubleshooting:
- If the ping command times out it means the security configuration wasn't correctly set up.
    - Ensure the All traffic (on all ports and all protocols) rule is added to the security group being used.
- If ssh into EC2 instance doesn't work:
    - Check the IP address, make sure it's the correct Public IP for the EC2 instance.
    - Check the path for the *.pem file.
    - Check if the permissions are correctly set on the *.pem file.

#### **Deploying the ML inference container on the Edge instance**
- Install docker on the EC2 instance using: [Install Docker Engine on Ubuntu](https://docs.docker.com/engine/install/ubuntu/).

Deployment of the ML inference container can be done with two methods.
1. Building and deploying the ML inference container on the Edge instance.
    - Refer to **Connect to the EC2 instance** section to find the Edge instance public ip and how to connect.
    - Copy from a local device the project folder to the edge instance. You can also try to download this project directly from the edge instance.
    - Build and deploy the ML inference container on the instance.
        ```sh
        docker build -t <IMAGE_NAME>:<IMAGE_TAG> .
        ```
        Mention the image name & tag name of your choice.

    - Check the Docker Images list with the command below.
    ```sh
    docker images
    ```
    - Run the ML inference container with the command following the method 2.

2. If the user has already built the container for ARM based Edge device, the same docker image can be pulled and launched on the cloud instance.
    - The ML inference image must be pushed from the local edge device to docker hub.
        - List the docker images to get information onto the ML inference image.
        ```sh
        docker images
        ```
        - Tag the ML inference image.
        ```sh
        docker tag <ML_inference_IMAGE_ID> <docker_hub_username>/<IMAGE_NAME>:<IMAGE_TAG>
        ```
        - Make sure you are logged in with your docker hub account and push the image.
        ```sh
        docker login
        docker push <docker_hub_username>/<IMAGE_NAME>
        ```
    - After pushing the docker image, we need to pull the image from the cloud instance. Refer to **Connect to the EC2 instance** section.
        ```sh
        docker pull <IMAGE_NAME>:<IMAGE_TAG>
        ```
- Run the ML inference container.
    ```sh
    docker run -it --network=host <IMAGE_NAME>:<IMAGE_TAG> -p <INF_PORT> -m <MODEL> -c <CPU_CORES_OF_DEVICE>
    ```
    Options:
    - -p: Port number where the ML inference container should run. Preferred port number is 8080 and should be given when running the Application container (cn-application project).
    - -m: Model name that should be loaded for inference. Currently **yolov3** and **tiny_yolov3** models are supported.
    - -c: Number of CPU cores available in the device. This argument is optional and has the default value of **4**.

    Note that when running the Application container, –ip option should be provided with the value of the public IP of the edge instance. To get that IP, refer to **Connect to the EC2 instance** section.


#### **Stop the ECS instance**
Users can stop the ECS instance if they no longer use it. This would stop billings for that specific instance. Please refer to AWS EC2 billings conditions for more details.
This will not erase any data on the instance. You can then restart it by following similar steps.

To stop the EC2 instance.
- Access the AWS Cloud EC2 console.
- Click on **Instances** to access the list of instances created.
- Select the instance you want to stop.
- Then, on the dropdown menu **Instance state**, choose **Stop Instance**.
The instance should stop and the **Instance state** column should update to **stop**.

### Alibaba cloud
#### **Create an ECS instance**
- Open the Alibaba Cloud and login.
- To access the Alibaba management console, in most cases, you must activate a cloud service beforehand. More information on [Use the Alibaba Cloud Management Console](https://www.alibabacloud.com/help/en/basics-for-beginners/latest/use-the-alibaba-cloud-management-console).
- On the Alibaba Cloud Management Console:
    - On the top search bar, search for **Elastic Compute Service**.
    - Click on **Elastic Compute Service** under **Consoles** from the results displayed.
- On the **Elastic Compute Service** console.
    - On the left-side navigation pane, click on **Overview**.
    - Click on **Create ECS Instance**.

        It will open the **Custom Launch** tab of the instance.

    - In **Basic Configurations**, the first step of the instance creation.
        - Select the billing method as **Pay-as-you-go** option.
        - In **Region** section.
            - Select the region of user choice. Preferred to use: **China Shanghai**.
            - Select the zone of user choice. Preferred to use: **Random** zone.
        - In **Instance Type** section, select **Type-based Selection** tab.
            - Select **Architecture** as **ARM**.
            - Select **Category** as **Compute Optimized** (preferred).

                Users can choose the ECS instance according to their need.
                - In the table, select **Compute Optimized Type c6r** family, **ecs.c6r.xlarge** instance type, **4v CPUs** and **8 GiB** memory.

                    Leave **Selected Instance Type Quantity** as 1 (preferred).

        - In **Image**, select **Public Image**.
            - From the dropdown menu, select **Ubuntu**.
            - Click on **Select a version** dropdown menu of input field and select **20.04 64- bit for ARM**.

        Let all the other configurations remain unchanged.

        Users can see the Total USD per Hour cost at the bottom of the page, close to the **Next** button.
        - Click on the **Next** button.
    - At the **Networking** step.
        - In **Network Type** section, click the **VPC** button and select an existing VPC. VPCs that are prefixed with [Default] are automatically created in the ECS console.

            Let all the other configurations remain unchanged and click on **Next**.
    - At the **System Configurations** step.
        - Select **Logon Credentials** as **Password**.

            Remember **Logon Username** is root by default.
        - Enter **Logon Password** of user choice and confirm password.
        - Change **Instance Name**.
        - Let all the other configurations remain unchanged, click on **Next**.
    - At the **Grouping** step, no need to fill any configurations. Click on **Next**.
    - At the **Preview** step, check all configurations selected. If user wants to edit, click the corresponding icon to modify the configuration.
        - Select **Automatic Release** and **Terms of Service**.
        - Click on the **Create Instance** button.

In the Created message, click **Console** to view the instance creation progress on the **Instances** page.

If the instance is created, it is in the **Running** state. Copy the public IP address of the instance for use when you want to connect to the ECS instance.

#### **Connect to the ECS instance**
- Open the Alibaba Cloud Page & Login with the Account Created.
- Search for **Elastic Compute Service** on Alibaba Cloud console.
- Click on **Elastic Compute Service** from the search result, it will open the Elastic Compute Service console.
- On the left-side navigation pane, in **Instances & Image**, click on **Instances**.
- Find the instance which you have created previously.
- In the **Actions** column, click **Connect**.
- Select **Connect** / **Sign in now** under **Workbench Connection**.
- Configure the logon credentials.
- Enter the user name as the root which was kept default during the creation of the ECS instance.
- For the password, provide the one given during the ECS instance creation.
- Click on **Ok**. This will provide the user with an instance terminal with which the user can build and deploy the ML inference container.

#### **Deploying the ML inference container on the Edge instance**
The user needs to install docker on the edge instance refer to [Install Docker Engine on Ubuntu](https://docs.docker.com/engine/install/ubuntu/) for installing docker on ubuntu.

Deployment of the ML inference container can be done in two methods.
1. Building and deploying the ML inference container on the Edge instance.
    - From the Alibaba Cloud console, navigate to the **ECS Console**.
    - On the left side menu under **Instances and Images** click on **Instances**.
    - Click on the instance created. In the **Basic Information** of the **Instance Details** tab, the user can see the Public IP.
    - Copy the public ip and update the below command to copy the project folder from the local device to the instance.
        ```sh
        scp -r <PATH_TO_PROJECT_FOLDER> root@<PUBLIC_IP_OF_EDGE_INSTANCE>:~/
        ```

        We can also directly dowload the project from the edge instance.
    - Provide the password as the one used to login to the edge instance.
    - Build and deploy the ML inference container on the instance.
        ```sh
        docker build -t <IMAGE_NAME>:<IMAGE_TAG> .
        ```
        Mention the image name & tag name of your choice.

    - Check the Docker Images list with the command below.
        ```sh
        docker images
        ```
    - Run the container with the command after the method 2.
2. If the user has already built the container for ARM based Edge device, the same docker image can be pulled and launched on the cloud instance.
    - The ML inference image must be pushed from the local edge device to docker hub.
        - List the docker images to get information onto the ML inference image.
        ```sh
        docker images
        ```
        - Tag the ML inference image.
        ```sh
        docker tag <ML_inference_IMAGE_ID> <docker_hub_username>/<IMAGE_NAME>:<IMAGE_TAG>
        ```
        - Make sure you are logged in with your docker hub account and push the image.
        ```sh
        docker login
        docker push <docker_hub_username>/<IMAGE_NAME>
        ```
    - After pushing the docker image, we need to pull the image to the cloud instance.
        ```sh
        docker pull <IMAGE_NAME>:<IMAGE_TAG>
        ```
- Run the ML inference container.
    ```sh
    docker run -it --network=host <IMAGE_NAME>:<IMAGE_TAG> -p <INF_PORT> -m <MODEL> -c <CPU_CORES_OF_DEVICE>
    ```
    Options:
    - -p: Port number where the ML inference container should run. Preferred port number is 8080 and should be given when running the Application container (cn-application project).
    - -m: Model name that should be loaded for inference. Currently **yolov3** and **tiny_yolov3** models are supported.
    - -c: Number of CPU cores available in the device. This argument is optional and has the default value of **4**.

Note that when running the Application container, –ip option should be provided with the value of the public ip of the edge instance. To get that IP:
- From the Alibaba Cloud console, navigate to the **ECS Console**, and access **Instances** page under **Instances and Images**.
- Click on the instance created. In the **Basic Information** of the **Instance Details** tab, the user can see the Public IP.

#### **Release the ECS instance**
- Users can release the instance if they no longer need it. After the instance is released, billing stops and its data cannot be restored.
- Note: Only pay-as-you-go instances can be released by using the method described in this section.
- From the Alibaba Cloud console navigate to **ECS console**.
- On the left side menu under **Instances and Images**, click on **Instances**.
- For the instance the user wants to release, in the **Actions** column, click on **More > Instance Status > Release**.
- In the Release dialog box, set **Release Mode** to **Release Now** and click **Next**.
- In the Release message, verify the instance to be released and click **OK**.

## What is next ?
- For deploying the use case, access cn-application and follow the README steps.

